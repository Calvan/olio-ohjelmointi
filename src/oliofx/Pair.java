/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliofx;

/**
 *
 * @author evanhala
 */
public class Pair<S, T> {
    private S s;
    private T t;
    
    public Pair(S foo, T t) {
        this.s = foo;
        this.t = t;
    }
    
    public void printValues() {
        System.out.println(s);
        System.out.println(t);
    
    }
    
}
