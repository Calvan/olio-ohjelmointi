/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliokurssi;

/**
 *
 * @author evanhala
 */
public class Cate extends Animal implements Amaze {
    
    public Cate() {
        super(1, "white", "Cate");
    }
    
    public Cate(int a, String c, String n) {
        super(a, c, n);
        System.out.println("Kisse has been born");
    }
    
    public String speak() {
        return "Miau!";
    }

    @Override
    public String wow() {
        return "Such code, much Java, wow";
    }
    
    
    
}
