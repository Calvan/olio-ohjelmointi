/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliokurssi;

import oliofx.SpeakException;

/**
 *
 * @author evanhala
 */
public abstract class Animal implements Amaze {
    protected int age;
    protected String color;
    protected String name;
    
    public Animal(int a, String c, String n) {
        age = a;
        color = c; 
        name = n;
        System.out.println("Animal has been constructed");
    }
    
    public abstract String speak();
    
    public String speak(String s) throws SpeakException {
        
        if (s.trim().length() < 3) {
            throw new SpeakException("Too short message", s.trim().length());
        }
        
        return s;
    }

    public String getColor() {
        return color;
    }

    public Animal setColor(String c) {
        color = c;
        return this;
    }
    
    public int getAge() {
        return age;
    }
    
    public Animal setAge(int age) {
        this.age = age;
        return this;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
}
