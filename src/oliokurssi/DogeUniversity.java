/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliokurssi;

import java.util.ArrayList;

/**
 *
 * @author evanhala
 */
public class DogeUniversity implements java.io.Serializable {
    private ArrayList<Doge> doges = new ArrayList<>();
    
    private String name;
    
    static private DogeUniversity dn = null;
    
    
    private DogeUniversity(String n) {
        name = n;
    }
    
    static public DogeUniversity getInstance() {
        if (dn == null)
            dn = new DogeUniversity("Doge University");
        
        return dn;
    }
    
    
    
    public void addDoge(Doge d) {
        doges.add(d);
    }
    
    public Doge getDoge(int i) {
        if(doges.size() > i)
            return doges.get(i);
        else
            return new Doge(-1, "ERROR", "");
    }
    
    public void cry() {
        
        for(int i = 0, max = doges.size(); i < max; i++) {
            System.out.println(doges.get(i).wow());
        }
        
        for(Doge d : doges) {
            System.out.println(d.speak());
        }
    
    
    }
    
    
    
}
