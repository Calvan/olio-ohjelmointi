/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oliokurssi;

/**
 *
 * @author evanhala
 */
public class DogeAcademy {
    private Doge[] doges = new Doge[5];
    private int dogeCount;
    
    private String name = "";
    
    public DogeAcademy(String n) {
        name = n;
        dogeCount = 0;
    
    }
    
    public void addDoge(Doge d) {
        if(dogeCount < 5) {
            doges[dogeCount] = d;
            dogeCount++;
        }
        else
            System.out.println("Dogela on täynnä! Such sad ;(");
    }
    
    public Doge getDoge(int i) {
        return doges[i];
    }
    
}
